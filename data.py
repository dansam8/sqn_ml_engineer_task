from pytorch_transformers import XLNetTokenizer
import pandas as pd
from sklearn.model_selection import train_test_split
import json

tokenizer = XLNetTokenizer.from_pretrained("xlnet-base-cased")


def prepare_data(filename="reviews_Musical_Instruments_5.json", max_len=256):
    """converts the json data to train.csv and test.csv as well as performs upsampling"""
    data = {"score": [], "tokens": []}
    with open(filename) as f:
        for line in f.readlines():
            j = json.loads(line)
            tokens = tokenizer.encode(j["reviewText"], add_special_tokens=True)
            if len(tokens) > max_len:
                tokens = tokens[:max_len + 1]
            data["tokens"].append(tokens)
            data["score"].append(j["overall"])

    df = pd.DataFrame(data, columns=["score", "tokens"])
    df_train_upsample = pd.DataFrame()
    df_test_upsample = pd.DataFrame()
    df_train, df_test = train_test_split(df, test_size=0.1)

    upsample_factors = [5, 5, 2, 1, 1]
    for i, factor in enumerate(upsample_factors):
        for g in range(factor):
            df_train_upsample = df_train_upsample.append(df_train[df_train["score"] == i + 1], ignore_index=True)

    for i, factor in enumerate(upsample_factors):
        for g in range(factor):
            df_test_upsample = df_test_upsample.append(df_test[df_test["score"] == i + 1], ignore_index=True)

    df_train_upsample.to_csv("train.csv", index=False)
    df_test_upsample.to_csv("test.csv", index=False)


prepare_data()
