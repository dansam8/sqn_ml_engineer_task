import torch
from pytorch_transformers import XLNetTokenizer, XLNetModel
from torch import nn
from torch.utils import data
import pandas as pd
from tensorboardX import SummaryWriter
import atexit
from functools import wraps


class DataSet(data.Dataset):
    def __init__(self, batch_size, test=False):
        super().__init__()
        self.batch_size = batch_size
        self.df = pd.read_csv("test.csv" if test else "train.csv").sample(frac=1)

    def get_weighting(self):
        weight = torch.zeros([5])
        for i in range(5):
            weight[i] = self.df[self.df["score"] == i + 1].__len__()

        weight = 1 / weight
        weight /= weight.mean()
        return weight

    def __len__(self):
        return self.df.__len__() // self.batch_size

    def __getitem__(self, item):
        """this function creates of size batch_size x biggest sample in current batch"""

        tensors = []
        for i in range(self.batch_size):
            tensors.append(torch.tensor(pd.eval(self.df.iloc[item * self.batch_size + i]["tokens"])))

        x = torch.zeros([self.batch_size,
                         max([t.size(0) for t in tensors])])

        for i in range(self.batch_size):
            x[i, (x.size(1) - tensors[i].size(0)):] = tensors[i]

        y = torch.empty(self.batch_size)
        for i in range(self.batch_size):
            y[i] = self.df.iloc[item * self.batch_size + i]["score"] - 1

        return x.long(), y.long()


class My_Net(nn.Module):

    def __init__(self):
        super().__init__()
        self.XLNet = XLNetModel.from_pretrained("xlnet-base-cased")
        self.fcl = nn.Linear(768, 5)
        self.iter = 0

    def forward(self, x):
        x = self.XLNet(x)[0]
        x = x[:, -1].view((-1, 768))  # one passes the last hidden state to the linear layer
        x = self.fcl(x)
        return x


class NetSaver:

    @staticmethod
    def save_net(obj, name):
        if (type(obj) == tuple):
            for i in range(len(obj)):
                with open(name[i], 'wb') as f:
                    torch.save(obj[i], f)
                print("saved net as", name[i])
        else:
            with open(name, "wb") as f:
                torch.save(obj, f)
            print("saver new as", name)

    @staticmethod
    def saver(net, save_name):
        """decorator to save the net anytime the program exits (to prevent loss in case of a runtime error"""

        def decorator(function):
            @wraps(function)
            def my_wrapper(*args):
                atexit.register(args[0].save_net, net, save_name)
                function(*args)

            return my_wrapper

        return decorator


class Util(NetSaver):

    def __init__(self, load_net_path=None):
        super().__init__()
        if load_net_path:
            with open(load_net_path, 'rb') as f:
                self.net = torch.load(f)
        else:
            self.net = My_Net()

    def test(self):
        data_set = DataSet(batch_size=1, test=True)
        data_loader = data.DataLoader(data_set, batch_size=1)
        outputs = torch.empty([data_set.__len__(), 5])
        labels = torch.empty(data_set.__len__())
        for b_id, (x, y) in enumerate(data_loader):
            print(b_id)
            with torch.no_grad():
                outputs[b_id] = self.net.forward(x.squeeze_(0))
                labels[b_id] = y

        labels_onehot = torch.nn.functional.one_hot(labels.long(), 5)
        output_onehot = torch.nn.functional.one_hot(outputs.argmax(1), 5)

        correct = (outputs.argmax(1).long() == labels.long()).long()
        true_positives = (labels_onehot * correct.unsqueeze(1)).transpose(1, 0).sum(1)

        incorrect = (outputs.argmax(1).long() != labels.long()).long()
        false_negatives = (labels_onehot * incorrect.unsqueeze(1)).transpose(1, 0).sum(1)
        false_positives = (output_onehot * incorrect.unsqueeze(1)).transpose(1, 0).sum(1)

        precision = true_positives.float() / (true_positives + false_positives).float()
        recall = true_positives.float() / (true_positives + false_negatives).float()
        f1 = 2 * ((precision * recall) / (precision + recall))
        print("Standard classifier metrics:")
        print("\tPrecision", precision.tolist())
        print("\tRecall", recall.tolist())
        print("\tF1", f1.tolist())

        '''
        however there are two problems relating to the relationship of the sentiment to the score
        A: the relationship is subjective.
        B: the sentiment is continuous and the score is discreet. 
        Therefore the classifier can not make perfect predictions so I've added a more lenient precision metric
        lenient_accuracy marking the sample correct if it is correct or adjacent to the correct score; correct over 
        total
        '''

        lenient_labels = torch.zeros_like(labels_onehot)
        for i in range(labels_onehot.size(0)):
            for g in range(labels_onehot.size(1)):
                if 1 == labels_onehot[i, g]:
                    lenient_labels[i, g] = 1
                elif g != 0 and labels_onehot[i, g - 1] == 1:
                    lenient_labels[i, g] = 1
                elif g != 4 and labels_onehot[i, g + 1] == 1:
                    lenient_labels[i, g] = 1

        correct = (torch.nn.functional.one_hot(outputs.argmax(1), 5) * lenient_labels.long()).long()
        accuracy = correct.sum().flaot() / labels.size(0)
        print("Lenient accuracy:", float(accuracy))

    def classify(self, text):
        self.net.eval()
        tokenizer = XLNetTokenizer.from_pretrained("xlnet-base-cased")
        tokens = torch.tensor(tokenizer.encode(text, add_special_tokens=True))
        if tokens.size(0) > 256:
            tokens = tokens[:256 + 1]
        with torch.no_grad():
            y = self.net.forward(tokens.unsqueeze(0))
        return int(y.argmax()) + 1, y.squeeze().tolist()

    def train(self, save_path, epochs, batch_size=3, gradient_accumulation_steps=10, lr=1e-5):

        try:
            with open(save_path, 'x') as _:
                pass
        except OSError as e:
            print("Save location warning", e)

        summary_writer = SummaryWriter('/'.join(__file__.split('/')[:-1]) + '/tb/' + save_path)
        data_set = DataSet(batch_size=batch_size)
        data_loader = data.DataLoader(data_set, batch_size=1, num_workers=1, shuffle=True)
        opt = torch.optim.Adam(self.net.parameters(), lr)
        loss_fn = nn.CrossEntropyLoss(weight=data_set.get_weighting())
        self.net.train()

        @NetSaver.saver(self.net, save_path)
        def t(self):

            for e in range(epochs):
                for b_id, (x, y) in enumerate(data_loader):
                    x.squeeze_(0)  # needed due to the data loader adding a dimension that was already added in the data
                    # set
                    y.squeeze_(0)

                    yi = self.net.forward(x)
                    loss = loss_fn(yi, y)
                    loss.backward()

                    # create an effective batch size of (batch_size * gradient_accumulation_steps)
                    if b_id % gradient_accumulation_steps == gradient_accumulation_steps - 1:
                        opt.step()
                        opt.zero_grad()

                        print("b_id:", b_id, "class", int(y[0].item()) + 1, yi[0].tolist())
                        summary_writer.add_scalar("loss", loss.item(), self.net.iter)
                        self.net.iter += 1

        t(self)


u = Util()
u.train("model", 1)
u.test()

"""
Standard classifier metrics:
	Precision tensor([0.9402, 0.8333, 0.8298, 0.5000, 0.8455])
	Recall tensor([0.9565, 0.9091, 0.5270, 0.5197, 0.8866])
	F1 tensor([0.9483, 0.8696, 0.6446, 0.5096, 0.8656])
Lenient accuracy: tensor(0.9750)

Standard classifier metrics:
	Precision tensor([0.7049, 0.3152, 0.2917, 0.3077, 0.8549])
	Recall tensor([0.4300, 0.5200, 0.4861, 0.3238, 0.6936])
	F1 tensor([0.5342, 0.3925, 0.3646, 0.3155, 0.7659])
	Lenient accuracy: tensor(0.9079)
"""
