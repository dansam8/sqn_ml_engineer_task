#SQN ML TASK

##Installation
```bash
git clone https://dansam8@bitbucket.org/dansam8/sqn_ml_engineer_task.git
pip install -r requirements.txt
```

##Usage
classifier.py contains the class Util with the following methods:
```python
__init__(self, load_net_path=None)
train(self, save_path, epochs, batch_size=3, gradient_accumulation_steps=10, lr=1e-5)
classify(self, text)
test(self)
```
data.py contains a function to convert the json data to cvs, as well as upsample and split into train and test

The model documentation can be found here:
https://docs.google.com/document/d/1sHLZGy5_4JnuMEWeXoSDdt5ttpnWQzxo2uyYGcRptrs/edit?usp=sharing